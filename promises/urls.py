from django.urls import include, path
from rest_framework.routers import DefaultRouter
from promises.views import PromiseViewSet, UserViewSet, UserAllViewSet

router = DefaultRouter()
router.register('promises', PromiseViewSet, base_name='promise')
router.register('users', UserViewSet)
router.register('userall', UserAllViewSet)

urlpatterns = [
    path('', include(router.urls))
]
