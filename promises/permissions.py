from rest_framework.permissions import BasePermission


class IsPromiseParticipant(BasePermission):
    def has_object_permission(self, request, view, obj):
        return (
            request.user and
            request.user.id in [obj.user1_id, obj.user2_id]
        )
