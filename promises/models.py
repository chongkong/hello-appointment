from django.contrib.auth.models import User
from django.db import models


class Promise(models.Model):
    sinceWhen = models.DateTimeField()
    tilWhen   = models.DateTimeField()
    user1     = models.ForeignKey(User, related_name='promises_as_inviter',
                                  on_delete=models.CASCADE)
    user2     = models.ForeignKey(User, related_name='promises_as_invitee',
                                  on_delete=models.CASCADE)
    created   = models.DateTimeField(auto_now_add=True)
