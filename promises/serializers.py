from rest_framework import serializers
import promises.models as models


class PromiseSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Promise
        fields = '__all__'
        read_only_fields = [
            'user1',
        ]

    def _get_field(self, data, field_name):
        return (
            data[field_name] if field_name in data
            else getattr(self.instance, field_name) if self.partial
            else None
        )

    def create(self, validated_data):
        validated_data['user1'] = self.context['request'].user
        promise = models.Promise.objects.create(**validated_data)
        return promise

    def validate(self, data):
        since_when = data['sinceWhen']
        til_when = data['tilWhen']
        user1 = self.context['request'].user
        user2 = data['user2']

        if since_when > til_when:
            raise serializers.ValidationError('sinceWhen > tilWhen')
        if user1 == user2:
            raise serializers.ValidationError('user1 == user2')
        return data


class PromisePartialSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Promise
        fields = '__all__'
        read_only_fields = [
            'user1',
            'user2'
        ]

    def _get_field(self, data, field_name):
        return (
            data[field_name] if field_name in data
            else getattr(self.instance, field_name) if self.partial
            else None
        )

    def update(self, instance, validated_data):
        for field in ['sinceWhen', 'tilWhen']:
            if field in validated_data:
                setattr(instance, field, validated_data[field])
        instance.save()
        return instance

    def validate(self, data):
        since_when = self._get_field(data, 'sinceWhen')
        til_when = self._get_field(data, 'tilWhen')
        if since_when > til_when:
            raise serializers.ValidationError('sinceWhen > tilWhen')
        return data


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = [
            'id',
            'username',
            'promises_as_inviter',
            'promises_as_invitee'
        ]


class UserAllSerializer(serializers.ModelSerializer):
    whole_promises = serializers.SerializerMethodField()

    class Meta:
        model = models.User
        fields = [
            'id',
            'username',
            'whole_promises'
        ]

    def get_whole_promises(self, obj):
        whole_promises = [
            *obj.promises_as_inviter.all().only('id'),
            *obj.promises_as_invitee.all().only('id')
        ]
        return [promise.id for promise in whole_promises]
