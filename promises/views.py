from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q

from promises.models import Promise, User
from promises.permissions import IsPromiseParticipant
from promises.serializers import PromiseSerializer, UserSerializer, UserAllSerializer, PromisePartialSerializer


class PromiseViewSet(viewsets.ModelViewSet):
    def get_serializer_class(self):
        if self.action in ['list', 'create']:
            return PromiseSerializer
        else:
            return PromisePartialSerializer

    def get_queryset(self):
        if self.action == 'list':
            user_id = self.request.user.id
            return Promise.objects.filter(Q(user1=user_id) | Q(user2=user_id))
        else:
            return Promise.objects.all()

    def get_permissions(self):
        if self.action in ['retrieve', 'update',
                           'partial_update', 'destroy']:
            return [IsAuthenticated(), IsPromiseParticipant()]
        else:
            return [IsAuthenticated()]


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = (User.objects.all()
                .prefetch_related('promises_as_inviter',
                                  'promises_as_invitee'))
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]


class UserAllViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = (User.objects.all()
                .prefetch_related('promises_as_inviter',
                                  'promises_as_invitee'))
    serializer_class = UserAllSerializer
    permission_classes = [IsAuthenticated]
